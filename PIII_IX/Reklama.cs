namespace PIII_IX
{
    public class Reklama
    {
        public string tekst;
        public Zainteresowania interes;
        public PrzedzialWiekowy przedzial;

        public Reklama(string tekst, Zainteresowania interes, PrzedzialWiekowy przedzial)
        {
            this.tekst = tekst;
            this.interes = interes;
            this.przedzial = przedzial;
        }
        
        
    }
    
}