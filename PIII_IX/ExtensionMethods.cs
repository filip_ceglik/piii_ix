using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace PIII_IX
{
    public static class ExtensionMethods
    {
        public static (int wynik, int reszta) DzieleniezReszta(this int liczba, int dzielnik)
        {
            return (liczba / dzielnik, liczba % dzielnik);
        }

        public static int ZliczWystapienia(this string wyraz, char znak, bool isCaseSensitive = true)
        {
            return wyraz.Count(x => x.Equals(znak));
        }

        public static int ZliczWystapienia(this string wyraz, string znak, bool isCaseSensitive = false)
        {
            return wyraz.Count(x => String.Equals(x.ToString(), wyraz, StringComparison.OrdinalIgnoreCase));
        }
        
        public static bool SprawdzReklameDlaDoroslych(this PrzedzialWiekowy przedzialWiekowy)
        {
            return przedzialWiekowy >= PrzedzialWiekowy.Dorosli;
        }
    }
}