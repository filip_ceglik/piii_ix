using System;

namespace PIII_IX
{
    [Flags]
    public enum PrzedzialWiekowy
    {
        Brak = 0,
        Dzieci = 1,
        Mlodziez = 2,
        Dorosli = 4,
        Starsi = 8
    }
    [Flags]
    public enum Zainteresowania
    {
        Informatyka,
        Elektronika,
        Motoryzacja,
        Sport
    }
}